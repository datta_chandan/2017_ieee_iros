# About IEEE IROS 2017

http://www.iros2017.org/contributing/instructions-for-authors
 
# Useful links


- https://ras.papercept.net/conferences/scripts/submissionwizard.pl
- https://ras.papercept.net/conferences/scripts/start.pl
- IEEE eXpress compliance: http://ras.papercept.net/conferences/scripts/pdftest.pl


# Changelog

